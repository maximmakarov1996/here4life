# Description

HERE4Life web app helps to analyze situation in the nearest hospitals. It calculates the optimal routes using HERE Waypoint, Routing and Isoline API and creates the optimal and effective routes for the delivery or transportation:
the patients to different hospitals according to available hospital beds. 
the medical equipment from storage to hospital.

HERE4Life solution enables to build the sophisticate navigation for Emergency vehicle and allows the delivery trucks with medical equipment.

# Data Features

Hospitals POIs from DIVI.
Number of COVID-19 Patients. Provide information such as number of patients in total per hospital. Colorizes according to workload (red, yellow, green). Create routes according to workload for emergency vehicle.
Online data of Intensive care unit (low or high)
Use cases:

Patients with COVID-19 can investigate which hospital is less loaded
Dispatchers (Ambulance or Equipment procurement) can analyze current situation in each hospital and create optimal routing
Ambulances
Warehouses

# Technical description of the product

We use HERE Data Hub to store and update information about hospitals in real time. As a data source we decided to use DIVI portal. Web-application we implemented using efficient set of tools including HERE JavaScript API. It’s possible to customize routes based on traffic information and type of vehicle. To solve last mile problem, we implemented functionality of Waypoint API that helps to order up to 100 points. 

And finally, great functionality depends on isochrones which HERE Location Suite also supports. 
Governmental or private organizations can use value added services for workstream optimization. Data contributors are nationwide government aggregators.

The idea is applicable in various business areas, such as emergency monitoring. 
An international supplier of medical equipment is already interested in the solution.

# Installation

1. Check if you have NodeJS installed:

```bash
npm -v
```

2. Check if you have connection to VPN.

3. Use LUI tutorial to configure connection to internal design system.

4. Install requirements:

```bash
npm install
```

5. Start local develompent server:

```bash
npm start
```

