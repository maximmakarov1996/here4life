import React from 'react'
import PropTypes from 'prop-types'

import {
    CustomInput,
} from 'reactstrap'

import chevronDown from './icons/LUI-icon-pd-chevron_down-solid-16.svg'

import 'bootstrap/dist/css/bootstrap.min.css'
import './style.css'

export const Routing = ({options, onChangeIsolineRange, clickRadioButtonParameter, dropdownClick}) => {

    return (
        <div className="parameters">
            <div className="routing-parameter-block">
                <div className="routing-item-row">
                    <div className="routing-item-column">
                        {
                            options.dropdown.waypoints ?
                                <img className="chevron-down-icon" onClick={() => dropdownClick("waypoints")} src={chevronDown} ></img>
                            :
                                <img className="chevron-right-icon" onClick={() => dropdownClick("waypoints")} src={chevronDown} ></img>
                        }
                    </div>
                    <div className="label-isoline"> Waypoints parameters </div>
                </div>
                <div className={options.dropdown.waypoints ? "routing-item-column parameters-content" : "d-none"}>
                    <div >
                        <lui-label>For routing:<br/>1. Right-click on the map <br/> 2. Select “Add waypoint” <br/> 3. Add starting and destination points </lui-label>
                        <br/>
                        <lui-label>Transport</lui-label>
                        <lui-radiobutton-group columns="2">
                            { 
                                options.waypoints.transport === "car" ? 
                                    <lui-radiobutton checked >Car</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("waypoints", "transport", "car")}>Car</lui-radiobutton>
                            }
                            { 
                                options.waypoints.transport === "pedestrian" ? 
                                    <lui-radiobutton checked >Pedestrian</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("waypoints", "transport", "pedestrian")}>Pedestrian</lui-radiobutton>
                            }
                        </lui-radiobutton-group>
                        
                        <lui-label>Traffic</lui-label>
                        <lui-radiobutton-group columns="2">
                            { 
                                options.waypoints.traffic === "enabled" ? 
                                    <lui-radiobutton checked>Enabled</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("waypoints", "traffic", "enabled")}>Enabled</lui-radiobutton>
                            }
                            { 
                                options.waypoints.traffic === "disabled" ? 
                                    <lui-radiobutton checked>Disabled</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("waypoints", "traffic", "disabled")}>Disabled</lui-radiobutton>
                            }
                        </lui-radiobutton-group>
                    </div>
                    <div className={options.waypoints.geometry.length !== 0 ? "d-block" : "d-none"}>
                            <a href={options.waypoints.url} target="_blank">
                                <lui-button >Open navigation in HERE WeGo</lui-button>
                            </a>
                        </div>
                </div>
            </div>

            <div className="routing-parameter-block">
                <div className="routing-item-row">
                    <div className="routing-item-column">
                        {
                            options.dropdown.isoline ?
                                <img className="chevron-down-icon" onClick={() => dropdownClick("isoline")} src={chevronDown} ></img>
                            :
                                <img className="chevron-right-icon" onClick={() => dropdownClick("isoline")} src={chevronDown} ></img>
                        }
                    </div>
                    <div className="label-isoline"> Isoline parameters </div>
                </div>
                <div className={options.dropdown.isoline ? "routing-item-column parameters-content" : "d-none"}>
                    <lui-label>For isoline:<br/>1. Right-click on the map <br/> 2. Select “Calculate isoline” <br/> </lui-label>
                    <br/>
                    <div className="routing-item-row space-between">
                        <div className="routing-item-column">
                            <lui-label>Range</lui-label>
                        </div>
                        <div className="routing-item-column">
                            <lui-label>
                                {options.isoline.type === "time" ? `${options.isoline.range / 60} min` : `${options.isoline.range} m`}
                            </lui-label>
                        </div>
                    </div>
                    <div className="routing-item-row">
                        <CustomInput 
                            className="slider" 
                            type="range" 
                            id="isoline-range" 
                            name="isolineRange"
                            min="60"
                            step="120"
                            max="1860"
                            value={options.isoline.range}
                            onChange={evt => onChangeIsolineRange(evt.target.value)} />    
                    </div>
                    <div className="parameters-content-item">
                        <lui-label>Transport</lui-label>
                        <lui-radiobutton-group columns="2">
                            { 
                                options.isoline.transport === "car" ? 
                                    <lui-radiobutton checked >Car</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("isoline", "transport", "car")}>Car</lui-radiobutton>
                            }
                            { 
                                options.isoline.transport === "pedestrian" ? 
                                    <lui-radiobutton checked >Pedestrian</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("isoline", "transport", "pedestrian")}>Pedestrian</lui-radiobutton>
                            }
                        </lui-radiobutton-group>

                        <lui-label>Mode</lui-label>
                        <lui-radiobutton-group columns="2">
                            { 
                                options.isoline.type === "time" ? 
                                    <lui-radiobutton checked >Time</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("isoline", "type", "time")}>Time</lui-radiobutton>
                            }
                            { 
                                options.isoline.type === "distance" ? 
                                    <lui-radiobutton checked >Distance</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("isoline", "type", "distance")}>Distance</lui-radiobutton>
                            }
                        </lui-radiobutton-group>
                        
                        <lui-label>Traffic</lui-label>
                        <lui-radiobutton-group columns="2">
                            { 
                                options.isoline.traffic === "enabled" ? 
                                    <lui-radiobutton checked>Enabled</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("isoline", "traffic", "enabled")}>Enabled</lui-radiobutton>
                            }
                            { 
                                options.isoline.traffic === "disabled" ? 
                                    <lui-radiobutton checked>Disabled</lui-radiobutton>
                                : 
                                    <lui-radiobutton onClick={() => clickRadioButtonParameter("isoline", "traffic", "disabled")}>Disabled</lui-radiobutton>
                            }
                        </lui-radiobutton-group>
                    </div>
                </div>
            </div>

        </div>
    )
}

Routing.propTypes = {
    options: PropTypes.object,
    onChangeIsolineRange: PropTypes.func,
    clickCheckboxParameter: PropTypes.func,
    clickOptionDropdown: PropTypes.func
}